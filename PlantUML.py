import sublime
import sublime_plugin

import os
import subprocess

target_extension_map = {
    'svg': '.svg',
    'pdf': '.pdf',
    'png': '.png',
}

class PlantUmlCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.settings = sublime.load_settings('PlantUML.sublime-settings')
        self.available_targets = list(target_extension_map.keys())
        self.view.window().show_quick_panel(self.available_targets, self.execute_plantuml_command)

    def execute_plantuml_command(self, selected_target):
        if selected_target == -1:
            return

        jar_file = self.settings.get('plantuml_jar')
        if jar_file is None:
            sublime.error_message('PlantUML jar file not set. Configure in settings for the PlantUML plugin.')
            print('plantuml_jar not set')
            return

        cmd = [
            'java',
            '-jar', 
            jar_file, 
            '-failfast2', 
            '-pipe', 
            '-t%s' % self.available_targets[selected_target],
            ]

        region = sublime.Region(0, self.view.size())
        contents = self.view.substr(region)

        buffer_file_name = self.view.file_name()
        if buffer_file_name:
            working_dir = os.path.dirname(buffer_file_name)
            file_name = os.path.splitext(buffer_file_name)[0]
        else:
            sublime.error_message('No file name available, save the buffer before running PlantUML.')
            print('No file name available, save the buffer before running PlantUML.')
            return

        with open(file_name + target_extension_map[self.available_targets[selected_target]], 'w') as output:
            process = subprocess.Popen(
                cmd, 
                shell=False, cwd=working_dir,
                stdin=subprocess.PIPE,
                stdout=output,
                stderr=subprocess.PIPE)
    
            stdout, stderr = process.communicate(contents.encode('utf-8'))
    
            if stderr:
                stderr_content = stderr.decode('utf-8').strip()
    
                sublime.error_message('\n'.join([
                    'running ' + ' '.join(cmd),
                    stderr_content]))
    
                return